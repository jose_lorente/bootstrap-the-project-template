The Project Bootstrap Template
==============================

A full multipurpose HTML 5 template for bootstrap. A license must be owned in 
order to use this package. Visit [{wrap}boostrap](https://wrapbootstrap.com/theme/the-project-multipurpose-template-WB0F82581) 
in order to buy the license.

## Installation

Include the package as dependency under the bower.json file.

```json
"dependencies": {
    ...
    "bootstrap-the-project-template": "~1.0"
}
```

or install the package directly

```bash
$ bower install bootstrap-the-project-template
```

## Usage

Visit [The Project Template Live Preview](http://wrapbootstrap.com/preview/WB0F82581) 
in order to see complete template options and components.

## License 
Copyright &copy; 2014 htmlcoder.me